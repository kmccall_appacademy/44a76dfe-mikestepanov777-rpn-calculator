class RPNCalculator
  def initialize
    @stack = []
  end

  def push(obj)
    @stack.unshift(obj)
  end

  def plus
    calculate!(:+)
  end

  def minus
    calculate!(:-)
  end

  def divide
    calculate!(:/)
  end

  def times
    calculate!(:*)
  end

  def value
    @value
  end

  def calculate!(sym = nil)
    raise("calculator is empty") if @stack.size == 0
    unless sym.nil?
      @value = [@stack.shift.to_f, @stack.shift].reverse.inject(sym)
      push(@value)
    else
      arr = slice_to_calc!
      @value = [arr[0].to_f, arr[1]].inject(arr[2])
      @stack.insert(arr.last, @value)
    end
  end

  def slice_to_calc!
    idx_sym = @stack.index {|chr| chr.class == Symbol}
    arr = @stack.slice!(idx_sym - 2..idx_sym) << idx_sym - 2
  end

  def tokens(str)
    str.split(" ").map{|chr| chr.scan(/\d/).size == 1 ? chr.to_i : chr.to_sym}
  end

  def evaluate(str)
    @stack = tokens(str)
    until @stack.size == 1
      calculate!
    end
    value
  end
end
